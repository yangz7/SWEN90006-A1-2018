package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.util.Date;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
    // EC 1
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void noReturnTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC01_no_return.s");
      Machine m = new Machine();
      m.execute(lines);
  } 
  
  // EC 2
  @Test(expected = swen90006.machine.InvalidInstructionException.class) 
  public void wrongRegisterTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC02_wrong_register.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // EC 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class) 
  public void valueTooLowTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC03_valueTooLow.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // EC 4
  @Test(expected = swen90006.machine.InvalidInstructionException.class) 
  public void valueTooLargeTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC04_valueTooLarge.s");
      Machine m = new Machine();
      m.execute(lines);
  }

  // EC 5
  @Test
  public void additionTest()
  {
	  final List<String> lines = readInstructions("examples/EC05_add_100_10.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 1100);
  }
  
  // EC 6
  @Test
  public void subtractionTest()
  {
	  final List<String> lines = readInstructions("examples/EC06_sub_88_77.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 11);
  }
  
  // EC 7
  @Test
  public void multiplicationTest()
  {
	  final List<String> lines = readInstructions("examples/EC07_mul_12_21.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 252);
  }
  
  // EC 8
  @Test
  public void divisionTest()
  {
	  final List<String> lines = readInstructions("examples/EC08_div_99_33.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 3);
  }
  
  // EC 9
  @Test 
  public void divByZeroTest()
  {
	  final List<String> lines = readInstructions("examples/EC09_dividing_zero.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 5);
  }
  
  // EC 10
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jumpVeryNegativeTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC10_jump_back.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // EC 11
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jumpVeryFarTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC11_jump_forward.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // EC 12
  @Test 
  public void jumpToTest()
  {
	  final List<String> lines = readInstructions("examples/EC12_jumpToReturn.s");
      Machine m = new Machine();
      assertEquals(21, m.execute(lines));
  }
  

  // EC 13
  @Test
  public void infiniteLoopTest()
  {
	  int[] finish = new int[1];
	  finish[0] = 0;
      Thread program_T = new Thread (new Runnable() {
    	  public void run() {
    		  final List<String> lines = readInstructions("examples/EC13_jumpZero.s");
			  Machine m = new Machine();
			  m.execute(lines);
			  finish[0] = 1;
    	  }
      });
      program_T.start();
      
      try {
		Thread.sleep(5000); //Change to longer time to check
      } catch (InterruptedException e) {
    	  e.printStackTrace();
      }
      program_T.interrupt();
      assertEquals(0, finish[0]);
  }

  
  // EC 14
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jzNegativeJumpTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC14_jz_h2jzNeg3.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // EC 15
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jzPositiveJumpTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/EC15_jz_h2jzPos5.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // EC 16
  @Test
  public void jzConditionNotMetTest()
  {
	  final List<String> lines = readInstructions("examples/EC16_jz_R0IsNotZero.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), -5);
  }
  
  // EC 17
  @Test
  public void jzConditionMetTest()
  {
	  final List<String> lines = readInstructions("examples/EC17_jz_R0IsZero.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 21);
  }
  
  // EC 18
  @Test
  public void str_ldr_LowAddressTest()
  {
	  final List<String> lines = readInstructions("examples/EC18_strTooLow.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
  }
  
  // EC 19
  @Test
  public void str_ldr_LargeAddressTest()
  {
	  final List<String> lines = readInstructions("examples/EC19_strTooLarge.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
  }
  
  // EC 20
  @Test
  public void str_ldr_AddressTest()
  {
	  final List<String> lines = readInstructions("examples/EC20_str_ldr_21.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), 21);
  }
}
