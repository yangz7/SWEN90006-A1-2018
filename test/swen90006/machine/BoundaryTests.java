package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
    // BV EC 2 register out of bounds
  @Test(expected = swen90006.machine.InvalidInstructionException.class) 
  public void register31Test()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_02.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC3 
  @Test(expected = swen90006.machine.InvalidInstructionException.class) 
  public void movPastNegativeTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_03.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC4
  @Test(expected = swen90006.machine.InvalidInstructionException.class) 
  public void movPastPositiveTest()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_04.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC8
  @Test 
  public void divNominatorLarge()
  {
	  final List<String> lines = readInstructions("examples/BV_08.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), -65535);
  }
  	
  // BV EC9
  @Test
  public void divDenominatorZero()
  {
	  final List<String> lines = readInstructions("examples/BV_09.s");
      Machine m = new Machine();
      assertEquals(m.execute(lines), -65535);
  }
  
  // BV EC10
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jmpNegative1()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_10.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC11
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jmpPassPos()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_11.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC12
  public void jmpToEnd()
  {
	  final List<String> lines = readInstructions("examples/BV_12.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC13
  @Test
  public void jumpToInf()
  {
	  int[] finish = new int[1];
	  finish[0] = 0;
      Thread program_T = new Thread (new Runnable() {
    	  public void run() {
    		  final List<String> lines = readInstructions("examples/BV_13.s");
			  Machine m = new Machine();
			  m.execute(lines);
			  finish[0] = 1;
    	  }
      });
      program_T.start();
      
      try {
		Thread.sleep(5000); //Change to longer time to check
      } catch (InterruptedException e) {
    	  e.printStackTrace();
      }
      program_T.interrupt();
      assertEquals(0, finish[0]);
  }
  
  // BV EC14 invalid
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jzNegative1()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_14.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC15 invalid
  @Test(expected = swen90006.machine.NoReturnValueException.class) 
  public void jzPassLen()
		  throws Throwable
  {
	  final List<String> lines = readInstructions("examples/BV_15.s");
      Machine m = new Machine();
      m.execute(lines);
  }
  
  // BV EC15 EC16
  @Test
  public void jzRegisterNot0()
  {
    final List<String> lines = readInstructions("examples/BV_1516.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 21);
  }
  // BV EC14 EC16
  @Test
  public void jzToInf()
  {
	  int[] finish = new int[1];
	  finish[0] = 0;
      Thread program_T = new Thread (new Runnable() {
    	  public void run() {
    		  final List<String> lines = readInstructions("examples/BV_1416.s");
			  Machine m = new Machine();
			  m.execute(lines);
			  finish[0] = 1;
    	  }
      });
      program_T.start();
      
      try {
		Thread.sleep(5000); //Change to longer time to check
      } catch (InterruptedException e) {
    	  e.printStackTrace();
      }
      program_T.interrupt();
      assertEquals(0, finish[0]);
  }
  // BV EC18 EC19
  @Test
  public void str_ldr_past_boundaries()
  {
    final List<String> lines = readInstructions("examples/BV_1819.s");
    Machine m = new Machine();
    assertEquals(0, m.execute(lines));
  }
  
  // BV EC20
  @Test
  public void str_ldr_at_boundries()
  {
    final List<String> lines = readInstructions("examples/BV_20.s");
    Machine m = new Machine();
    assertEquals(21, m.execute(lines));
  }
}
